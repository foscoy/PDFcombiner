PDF Merger
============
Simple GUI app to merge pdf files.

This script opens a window where the user can load and organise a list
of PDF files that can then be merged into a single file.

Requirements
============
Python 
tkinter
PyPDF2

Notes
============
Very simple and straight forward GUI and script inspired by "Automate 
the Boring Stuff." The script is functional but it still needs work in
order to handle errors well.
