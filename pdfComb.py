from tkinter import *
from tkinter import filedialog, END
import os
from PyPDF2 import PdfFileMerger

win = Tk()

FILES_LIST = []

# Load files from FILES_LIST into file_listbox
def displayFiles ():
    file_listbox.delete(0,(file_listbox.size()-1))
    for i in FILES_LIST:
        file_listbox.insert(END, os.path.split(i)[1])

# Move list item up
def move_file_up():
    global FILES_LIST
    FILES_LIST.insert(file_listbox.curselection()[0]-1,
                      FILES_LIST[file_listbox.curselection()[0]])
    FILES_LIST.pop(file_listbox.curselection()[0]+1)
    displayFiles()
    
# Move list item down
def move_file_down():
    FILES_LIST.insert(file_listbox.curselection()[0]+2,
                      FILES_LIST[file_listbox.curselection()[0]])
    FILES_LIST.pop(file_listbox.curselection()[0])
    displayFiles()
    
# Load file to merge
def loadFile():
    pdfFile = ''
    pdfFile = filedialog.askopenfilename(initialdir='~/Documents/',
                                         defaultextension='*.pdf',
                                         filetypes=[('PDF', '*.pdf')])
    FILES_LIST.append(pdfFile)
    displayFiles()
    print(FILES_LIST)

# Merge files in FILES_LIST list
def merge_files():
    merger= PdfFileMerger()
    for x in FILES_LIST:
        merger.append(x)
    dir_name = filedialog.asksaveasfilename(defaultextension='*.pdf',
                                            filetypes=[('PDF', '*.pdf')])
    merger.write(dir_name)

# Clear list of files
def cl_list():
    global FILES_LIST
    FILES_LIST = []
    displayFiles()

# Frames to organize widgets
top_left_frame = Frame(win, bd=2, relief=FLAT, height=350, width=100)
top_right_frame = Frame(win, height=350, width=730)
bottom_frame = Frame(win, height=50, width=820)

# Button to manipulate file list
load_files = Button(top_left_frame, text='Load PDF', command=loadFile)
move_up = Button(top_left_frame, text='/\\', command=move_file_up)
move_down = Button(top_left_frame, text='\\/', command=move_file_down)

# Listbox to list files in FILES_LIST
file_listbox = Listbox(top_right_frame, bg='white', height=20, width=40)

# Bottom frame, widgets merge pdf files
clear_list = Button(bottom_frame, text='Clear List', command=cl_list)
merge_button = Button(bottom_frame, text='Merge PDFs', command=merge_files)
quit_buttons = Button(bottom_frame, text='Quit', command=quit)

bottom_frame.pack(side='bottom', padx=1, pady=1, fill=BOTH, expand=1)
top_left_frame.pack(side='left', padx=1, pady=1)
top_right_frame.pack(side='left', padx=1, pady=1)

# Top Left frame
top_left_frame.rowconfigure(0, minsize=100, pad=1, weight=2)
top_left_frame.rowconfigure(1, minsize=20, pad=1, weight=1)
top_left_frame.rowconfigure(2, minsize=20, pad=1, weight=1)
top_left_frame.rowconfigure(3, minsize=100, pad=1, weight=1)
top_left_frame.columnconfigure(0, minsize=10, pad=1)

load_files.grid(column=0, row=0, sticky=N)
move_up.grid(column=0, row=1, sticky=E)
move_down.grid(column=0, row=2, sticky=E)

file_listbox.pack(side='left', expand=1, fill=BOTH)

clear_list.pack(side='left', padx=20, anchor=CENTER)
merge_button.pack(side='left', padx=48, anchor=CENTER)
quit_buttons.pack(side='right', padx=20, anchor=CENTER)

win.title("PDF Combiner")
win.mainloop()